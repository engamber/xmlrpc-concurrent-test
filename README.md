# XML-RPC example for concurrent synchronous calls

## Build
The C++ server requires `xmlrpc-c-devel` and it's dependencies.

From https://stackoverflow.com/questions/4138310/xmlrpc-server-compilation-problem-in-linux

    gcc -Wall -g -c -I. $(xmlrpc-c-config c++2 abyss-server --cflags) server.cpp
    gcc -Wall -g -o server server.o $(xmlrpc-c-config c++2 abyss-server --libs) -lstdc++

Super minimal working build command:
    
    gcc -o server server.cpp $(xmlrpc-c-config c++2 abyss-server --libs) -lstdc++

## Running

Execute the python or C++ server:

    python3 server.py

or

    ./server
    
then execute the client process:

    python3 client.py