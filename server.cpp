#define WIN32_LEAN_AND_MEAN /* required by xmlrpc-c/server_abyss.hpp */

#include <cassert>
#include <stdexcept>
#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

using namespace std;

#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>
#include <xmlrpc-c/server_abyss.hpp>

#ifdef _WIN32
#define SLEEP(seconds) SleepEx(seconds * 1000);
#else
#define SLEEP(seconds) sleep(seconds);
#endif

class is_even : public xmlrpc_c::method
{
public:
  is_even()
  {
    // signature and help strings are documentation -- the client
    // can query this information with a system.methodSignature and
    // system.methodHelp RPC.
    this->_signature = "b:i";
    // method's result is bool and one argument is integer
    this->_help = "Returns true if the argument is even";
  }
  void
  execute(xmlrpc_c::paramList const &paramList,
          xmlrpc_c::value *const retvalP)
  {

    int const n(paramList.getInt(0));

    paramList.verifyEnd(1);

    *retvalP = xmlrpc_c::value_boolean(n % 2 == 0);

    // Sometimes, make it look hard (so client can see what it's like
    // to do an RPC that takes a while).
    if (n == 3)
      throw runtime_error("Ahahaa");
      SLEEP(2);
  }
};

class is_odd : public xmlrpc_c::method
{
public:
  is_odd()
  {
    // signature and help strings are documentation -- the client
    // can query this information with a system.methodSignature and
    // system.methodHelp RPC.
    this->_signature = "b:i";
    // method's result is bool and one argument is integer
    this->_help = "Returns true is the argument is odd";
  }
  void
  execute(xmlrpc_c::paramList const &paramList,
          xmlrpc_c::value *const retvalP)
  {

    int const n(paramList.getInt(0));

    paramList.verifyEnd(1);

    *retvalP = xmlrpc_c::value_boolean(n % 2 == 1);

    // Sometimes, make it look hard (so client can see what it's like
    // to do an RPC that takes a while).
    if (n == 3)
      SLEEP(2);
  }
};

int main(int const, const char **const)
{
  try
  {
    xmlrpc_c::registry myRegistry;

    xmlrpc_c::methodPtr const p_is_even(new is_even);
    xmlrpc_c::methodPtr const p_is_odd(new is_odd);

    myRegistry.addMethod("is_even", p_is_even);
    myRegistry.addMethod("is_odd", p_is_odd);

    xmlrpc_c::serverAbyss myAbyssServer(
        xmlrpc_c::serverAbyss::constrOpt()
            .registryP(&myRegistry)
            .portNumber(8000));

    myAbyssServer.run();
    // xmlrpc_c::serverAbyss.run() never returns
    assert(false);
  }
  catch (exception const &e)
  {
    cerr << "Something failed.  " << e.what() << endl;
  }
  return 0;
}