from xmlrpc.client import ServerProxy
import threading
import time

def submit_even():
  proxy = ServerProxy("http://localhost:8000/RPC2")
  try:
    print("3 is even: %s" % str(proxy.is_even(3)))
  except Exception as e:
    print('Oh no!', e)

def submit_odd():
  proxy = ServerProxy("http://localhost:8000/RPC2")
  try:
    print("3 is even: %s" % str(proxy.is_odd(3)))
  except Exception as e:
    print('Oh no!', e)

threads = []

threads.append(threading.Thread(target=submit_even))
# threads.append(threading.Thread(target=lambda: print("100 is even: %s" % str(proxy.is_even(100)))))
threads.append(threading.Thread(target=submit_odd))
# threads.append(threading.Thread(target=lambda: print("100 is odd: %s" % str(proxy.is_odd(100)))))

start = time.time()
for t in threads:
  t.start()
for t in threads:
  t.join()
stop = time.time()
print(stop-start)