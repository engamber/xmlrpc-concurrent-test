from xmlrpc.server import SimpleXMLRPCServer
from socketserver import ThreadingMixIn
from time import sleep

def is_even(n):
  sleep(2)
  if n == 3:
    raise Exception('Just raising because of {}'.format(n))
  return n % 2 == 0

def is_odd(n):
  sleep(2)
  return n % 2 == 1

class RPCThreading(ThreadingMixIn, SimpleXMLRPCServer):
  pass

server = RPCThreading(("localhost", 8000))
print("Listening on port 8000...")
server.register_function(is_even, "is_even")
server.register_function(is_odd, "is_odd")
server.serve_forever()